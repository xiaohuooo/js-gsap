/**!
 * Created by 赫连勃格 on 2023/01/01.
 * 微信:18900220083
 * QQ:215611388
 * next.UICUT.com
 */
"use strict";

// 通用：弹框关闭
$(document).on('click', '.uc-alert .btn-close,.uc-alert .over-close,.uc-alert .js_cancel', function (event) {
  event.preventDefault();
  $(this).parents(".uc-alert").fadeOut('300', function () {
    $(this).removeClass('uc-show');
  });
});
// 移动菜单
var alertProductMove = function alertProductMove() {
  var type = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : 'in';
  if (type == 'in') {
    $(".alert-product .box").css("left", "-60%");
    $(".alert-product").css("display", "block");
    $(".alert-product .box").animate({
      'left': '0%'
    }, 200);
    $(".header-phone .btn-product").addClass('on');
  } else {
    $(".alert-product .box").animate({
      'left': '-60%'
    }, 200, function () {
      $(".alert-product").css("display", "none");
    });
    $(".header-phone .btn-product").removeClass('on');
  }
};
var alertMenuMove = function alertMenuMove() {
  var type = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : 'in';
  if (type == 'in') {
    $(".alert-menu .box").css("right", "-60%");
    $(".alert-menu").css("display", "block");
    $(".alert-menu .box").animate({
      'right': '0%'
    }, 200);
    $(".header-phone .btn-menu").addClass('on');
  } else {
    $(".alert-menu .box").animate({
      'right': '-60%'
    }, 200, function () {
      $(".alert-menu").css("display", "none");
    });
    $(".header-phone .btn-menu").removeClass('on');
  }
};
$("body").on('click', '.header-phone .btn-menu', function (event) {
  event.preventDefault();
  alertProductMove('out');
  var type = $(this).hasClass("on") ? 'out' : 'in';
  // let type = $(".alert-menu .box").css('right')=='0px' ? 'out' : 'in'
  alertMenuMove(type);
});
$("body").on('click', '.header-phone .btn-product', function (event) {
  event.preventDefault();
  alertMenuMove('out');
  var type = $(this).hasClass("on") ? 'out' : 'in';
  // let type = $(".alert-product .box").css('left')=='0px' ? 'out' : 'in'
  alertProductMove(type);
});
$("body").on('click', '.alert-menu .over-close,.alert-product .over-close', function (event) {
  event.preventDefault();
  alertMenuMove('out');
  alertProductMove('out');
});
// 子菜单
$("body").on('click', '.alert-menu .hasSubMenu>a', function (event) {
  event.preventDefault();
  var _this = $(this).parent();
  if (_this.hasClass('on')) {
    _this.find('.subMenu').slideUp(300);
  } else {
    _this.find('.subMenu').slideDown(300);
    _this.siblings().find('.subMenu').slideUp(300);
  }
  _this.toggleClass('on');
});
// 验证码发送
var timeClock = function timeClock(cls) {
  var _this = cls;
  if (_this.hasClass('disabled')) {
    return false;
  } else {
    _this.addClass('disabled');
    var i = 59;
    var _int = null;
    var clock = function clock() {
      _this.text("\u91CD\u65B0\u53D1\u9001(".concat(i, ")"));
      i--;
      if (i < 0) {
        _this.removeClass('disabled');
        i = 59;
        _this.text("发送验证码(60)");
        clearInterval(_int);
      }
    };
    _int = setInterval(clock, 1000);
    return false;
  }
};
// 发送验证码
$("body").on('click', '.btn-yzm', function (event) {
  event.preventDefault();
  timeClock($('.btn-yzm'));
});
// 窗口变换
var oldW = $(window).width();
$(window).resize(function () {
  setTimeout(function () {
    var newW = $(window).width();
    if (oldW != newW) {
      location.reload();
    }
  }, 30);
});
if (!/msie [6|7|8|9]/i.test(navigator.userAgent)) {
  new WOW().init();
}
;